sdk install java 8.0.392-amzn

sudo chown gitpod:gitpod /opt
mkdir /opt/module /opt/software
cd /opt/software
wget "https://mirrors4.tuna.tsinghua.edu.cn/Adoptium/8/jdk/x64/alpine-linux/OpenJDK8U-jdk_x64_alpine-linux_hotspot_8u392b08.tar.gz"
wget "http://archive.apache.org/dist/hadoop/common/hadoop-3.1.3/hadoop-3.1.3.tar.gz"
wget "http://archive.apache.org/dist/hive/hive-3.1.2/apache-hive-3.1.2-bin.tar.gz"
tar -xzvf hadoop-3.1.3.tar.gz -C /opt/module
tar -xzvf apache-hive-3.1.2-bin.tar.gz -C /opt/module
mv /opt/module/hadoop-3.1.3/ /opt/module/hadoop
mv /opt/module/apache-hive-3.1.2-bin/ /opt/module/hive
# env config:
vi ~/.bashrc
export JAVA_HOME=/home/gitpod/.sdkman/candidates/java/current
export HADOOP_HOME=/opt/module/hadoop
export HIVE_HOME=/opt/module/hive
export PATH=$PATH:$HADOOP_HOME/bin:$HIVE_HOME/bin
export HADOOP_CLASSPATH=`hadoop classpath`
source ~/.bashrc
# test run:
hive --version

# hdfs namenode -format
# $HADOOP_HOME/sbin/start-dfs.sh
# hadoop fs -mkdir       /tmp
# hadoop fs -mkdir -p    /user/hive/warehouse
# hadoop fs -chmod g+w   /tmp
# hadoop fs -chmod g+w   /user/hive/warehouse

mv $HIVE_HOME/lib/guava-19.0.jar $HIVE_HOME/lib/guava-19.0.jar1
cp $HADOOP_HOME/share/hadoop/common/lib/guava-27.0-jre.jar $HIVE_HOME/lib/

cd /tmp
wget https://downloads.mysql.com/archives/get/p/3/file/mysql-connector-java-8.0.30.tar.gz
tar -xzvf mysql-connector-java-8.0.30.tar.gz
cp mysql-connector-java-8.0.30/mysql-connector-java-8.0.30.jar $HIVE_HOME/lib

cp /workspace/hive-test/hive-site.xml $HIVE_HOME/conf/hive-site.xml

mysql
> ALTER USER 'root'@'localhost' IDENTIFIED BY '123456';
> quit


schematool -dbType mysql -initSchema
